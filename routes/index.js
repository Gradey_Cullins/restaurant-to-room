var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    if(req.user) {
        return res.redirect('/orders');
    }
    var vm = {
        title: 'Login',
        error: req.flash('error') 
    };
    
    res.render('index', vm);
});

// demo 
router.get('/shit', function(req, res, next) {
    res.render('shit', { title: 'Login' });
});


module.exports = router;
