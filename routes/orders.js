var express = require('express');
var router = express.Router();

// add the 'restrict' function to the pipeline, ensuring that all users must be authenticated
// before having access to any destinations in the /orders route. The brute force alternative
// to this method is to check if the user is authenticated in each /order page. This solution is more elegant,
// and seems to follow suit with the pipeline / middleware style of design
var restrict = require('../auth/restrict');

router.get('/', restrict, function(req, res, next) {
    var vm = {
        title: "Place an order",
        orderId: req.session.orderId,
        // if req.user exists, set firstName equal to the first name field. If not, set the field to null
        firstName: req.user ? req.user.firstName : null 
    };
    res.render('orders/index', vm);
});

module.exports = router;
