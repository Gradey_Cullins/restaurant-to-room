var express = require('express');
var router = express.Router();
var passport = require('passport');
var config = require('../config');
var userService = require('../services/user-service');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/create', function(req, res, next) {
var vm = {
    title: 'create an account'
};
  res.render('users/create', vm);
});

router.post('/create', function(req, res, next) {
  userService.addUser(req.body, function(err) {
    if(err) {
      console.log(err);
      var vm = {
      title: 'create an account',
      input: req.body,
      error: err
    };
      delete vm.input.password; // the 'delete' keyword is typically used to remove properties from objects
  
      // by using 'return', we ensure that the redirect to the order page does not occur
      return res.render('users/create', vm);
    }
    req.login(req.body, function(err) {
      res.redirect('/orders');
    });
  });
});

router.post('/login', 
  function(req, res, next) {
    req.session.orderId = 12345;
    // if the user has checked the 'remember me' box, keep the user logged in for the next session
    if(req.body.rememberMe) {
      req.session.cookie.maxAge = config.cookieMaxAge;
    }
    next();
  },
  passport.authenticate('local', { 
  
    // the two below lines are part of the passport modules syntax
    failureRedirect: '/', // if the login fails, redirect to home page / root
    successRedirect: '/orders', // if the login is gud, den keep goin to teh finish line idyat
  
    failureFlash: 'Invalid credentials'
}));

router.get('/logout', function(req, res, next) {
  req.logout();
  req.session.destroy(); // remove the session cookie when the user logs out
  res.redirect('/');
});

module.exports = router;
