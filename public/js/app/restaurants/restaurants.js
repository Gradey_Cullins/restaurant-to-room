(function() {
    'use strict';
    
    angular
        .module('app')
        .controller('RestaurantsController', RestaurantsController);
     
    // make code minification-proof by injecting http module in the '$inject' array   
    RestaurantsController.$inject = ['$http'];     
    
    function RestaurantsController($http) {
        
        var vm = this;
        
        $http.get('/orders/api/restaurants')
            .then(function(response) {
                vm.restaurants = response.data;
            },
            function(reason) {
                console.log(reason);
            })
            .catch(function(err) {
                console.log(err);
            })
    }
}());